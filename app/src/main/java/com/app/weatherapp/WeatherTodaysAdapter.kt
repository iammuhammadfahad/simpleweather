package com.app.weatherapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.weatherapp.databinding.TodaysItemBinding
import com.app.weatherapp.ui.main.model.WeatherResponse
import com.squareup.picasso.Picasso

class WeatherTodaysAdapter(
    private val movies: WeatherResponse
) : RecyclerView.Adapter<WeatherTodaysAdapter.MoviesViewHolder>() {

    override fun getItemCount() = 8

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MoviesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.todays_item,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        val celsius: Double = movies.list[position].main.temp - 273.15
        holder.movieItemBinding.temp.text = celsius.toInt().toString() + " \u2103"

        try {
            val rain: Int = (movies.list[position].rain.h3 * 10).toInt()
            holder.movieItemBinding.rain.text = "$rain%"

        } catch (e: Exception) {
            holder.movieItemBinding.rain.text = "0%"

        }
        val time: Int = movies.list[position].dtTxt.split(" ")[1].split(":")[0].toInt()
        if (time > 12){
            holder.movieItemBinding.time.text = "$time PM"
        }else{
            holder.movieItemBinding.time.text = "$time AM"
        }

        val url1: String = Constraints.imageUrl + movies.list[position].weather[0].icon + "@4x.png"
        Picasso.get().load(url1).into(holder.movieItemBinding.image)
    }

    inner class MoviesViewHolder(
        val movieItemBinding: TodaysItemBinding
    ) : RecyclerView.ViewHolder(movieItemBinding.root)
}