package com.app.weatherapp.ui.main.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.weatherapp.ui.main.api.WeatherRepository
import com.app.weatherapp.ui.main.model.WeatherResponse
import com.tdsoftware.moviesharing.utils.Coroutines
import kotlinx.coroutines.Job

class WeatherViewModel(private val repository: WeatherRepository) : ViewModel() {

    private val _movies = MutableLiveData<WeatherResponse>()
    private lateinit var job: Job
    val movies: LiveData<WeatherResponse>
        get() = _movies

    fun getWeather() {
        job = Coroutines.getData(
            {
                repository.getCity1()
            }
        ) {
            _movies.value = it
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}