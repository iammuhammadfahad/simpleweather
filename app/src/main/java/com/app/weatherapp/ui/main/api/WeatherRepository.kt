package com.app.weatherapp.ui.main.api

class WeatherRepository(private val api: WeatherApi, private val city: String) : ApiRequest() {

    suspend fun getCity1() = apiRequest {
        api.getCity(
            city, "1f89da47fe4d0be6bbbf376af70bdb58"
        )
    }
}