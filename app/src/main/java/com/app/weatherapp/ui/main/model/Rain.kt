package com.app.weatherapp.ui.main.model

import com.google.gson.annotations.SerializedName

data class Rain(
    @SerializedName("3h")
    val h3: Double,

    @SerializedName("1h")
    val h1: Double
)