package com.app.weatherapp.ui.main.view

import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.weatherapp.ForecastAdapter
import com.app.weatherapp.WeatherTodaysAdapter
import com.app.weatherapp.R
import com.app.weatherapp.ui.main.api.WeatherApi
import com.app.weatherapp.ui.main.api.WeatherRepository
import com.app.weatherapp.ui.main.model.PageViewModel

class WeatherDataFragment : Fragment() {
    private lateinit var factory: WeatherViewModelFactory
    private lateinit var viewModel: WeatherViewModel
    private lateinit var recyclerVewWeather: RecyclerView
    private lateinit var recycleViewForecastWeather: RecyclerView
    private lateinit var progressCircular: ProgressBar
    private var horizontalLayout: LinearLayoutManager? = null
    private var verticalLayout: LinearLayoutManager? = null

    private lateinit var pageViewModel: PageViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val v: View = inflater.inflate(R.layout.weather_report_fragment, container, false)
        recyclerVewWeather = v.findViewById(R.id.recycler_view_weather)
        recycleViewForecastWeather = v.findViewById(R.id.recycler_view_forecast_weather)
        progressCircular = v.findViewById(R.id.progress_circular)

        val api = WeatherApi()
        val repository =
            WeatherRepository(api, arguments?.getString(ARG_SECTION_NAME) ?: "Rio De Janeiro")
        factory = WeatherViewModelFactory(repository)
        viewModel = ViewModelProvider(this, factory).get(WeatherViewModel::class.java)

        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
            viewModel.getWeather()
        }

        horizontalLayout = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        verticalLayout = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )

        recyclerVewWeather.layoutManager = horizontalLayout
        recyclerVewWeather.setHasFixedSize(true)

        viewModel.movies.observe(viewLifecycleOwner, Observer { movies ->
            recyclerVewWeather.also {
                it.layoutManager = horizontalLayout
                it.setHasFixedSize(true)
                it.adapter = WeatherTodaysAdapter(movies)
            }
        })

        recycleViewForecastWeather.layoutManager = verticalLayout
        recycleViewForecastWeather.setHasFixedSize(true)

        viewModel.movies.observe(viewLifecycleOwner, Observer { movies ->
            recycleViewForecastWeather.also {
                it.layoutManager = verticalLayout
                it.setHasFixedSize(true)
                it.adapter = ForecastAdapter(movies)
            }
            progressCircular.visibility = GONE
        })
        return v
    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        private const val ARG_SECTION_NAME = "section_name"

        @JvmStatic
        fun newInstance(sectionNumber: Int, i: String): WeatherDataFragment {
            return WeatherDataFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                    putString(ARG_SECTION_NAME, i)
                }
            }
        }
    }
}