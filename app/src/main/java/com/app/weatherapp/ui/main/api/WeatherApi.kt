package com.app.weatherapp.ui.main.api

import com.app.weatherapp.ui.main.model.WeatherResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    companion object {
        operator fun invoke(): WeatherApi {
            return Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherApi::class.java)
        }
    }

    @GET("forecast")
    suspend fun getCity(
        @Query("q") q: String,
        @Query("appid") regionCode: String,
    ): Response<WeatherResponse>
}