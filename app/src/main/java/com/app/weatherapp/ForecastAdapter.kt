package com.app.weatherapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.weatherapp.databinding.ForcastItemBinding
import com.app.weatherapp.ui.main.model.WeatherResponse
import com.squareup.picasso.Picasso

class ForecastAdapter(
    private val movies: WeatherResponse
) : RecyclerView.Adapter<ForecastAdapter.MoviesViewHolder>() {

    override fun getItemCount() = movies.list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MoviesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.forcast_item,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        val celsiusMax: Double = movies.list[position].main.tempMax - 273.15
        val celsiusMin: Double = movies.list[position].main.tempMin - 273.15
        holder.movieItemBinding.temp.text =
            celsiusMax.toInt().toString() + "\u2103 " + celsiusMin.toInt().toString() + "\u2103"

        holder.movieItemBinding.date.text = movies.list[position].dtTxt.split(" ")[0]

        holder.movieItemBinding.detail.text =
            movies.list[position].weather[0].main + " - " + movies.list[position].weather[0].description

        val url1: String = Constraints.imageUrl + movies.list[position].weather[0].icon + "@4x.png"
        Picasso.get().load(url1).into(holder.movieItemBinding.image)
    }

    inner class MoviesViewHolder(
        val movieItemBinding: ForcastItemBinding
    ) : RecyclerView.ViewHolder(movieItemBinding.root)
}